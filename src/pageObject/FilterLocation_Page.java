package pageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

	 public class FilterLocation_Page {

	 private static WebElement element = null;

	 public  Select drp_MaxPrice(WebDriver driver){

	    Select select = new Select (driver.findElement(By.id("maxPrice")));

	    return select;

	    }

	 public  Select drp_MinBedRoom(WebDriver driver){

	    Select select = new Select (driver.findElement(By.id("minBedrooms")));

	    return select;

	    }
	 
	 public  Select drp_MaxBedRoom(WebDriver driver){

		    Select select = new Select (driver.findElement(By.id("maxBedrooms")));

		    return select;

		    }
	 
	 public  WebElement btn_FindProperties(WebDriver driver){

		    element = driver.findElement(By.id("submit"));

		 return element;

		    }

	}


