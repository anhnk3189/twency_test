package automationFramework;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import junit.framework.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import pageObject.FilterLocation_Page;
import pageObject.SearchLocation_Page;

public class SearchTestCase{
	private static FirefoxDriver driver; 
 	WebElement element;
 	SearchLocation_Page searchLocationObj = new SearchLocation_Page();
 	FilterLocation_Page filterLocationObj = new FilterLocation_Page();
	@Before
	public void startBrowser() {
		driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.rightmove.co.uk/property-for-sale.html");
	}

	@Test
	public void execTests() {
		this.searchProperty();
	}
	
	@After
	public void tearDown() {
	    driver.quit();
	}
	
	public void searchProperty() {
		searchLocationObj.txt_SearchLocation(driver).sendKeys("Milton Keynes");
		searchLocationObj.btn_StartSearch(driver).click();
		filterLocationObj.drp_MaxPrice(driver).selectByVisibleText("300,000");
		filterLocationObj.drp_MinBedRoom(driver).selectByVisibleText("2");
		filterLocationObj.drp_MaxBedRoom(driver).selectByVisibleText("3");
		filterLocationObj.btn_FindProperties(driver).click();
	}
}