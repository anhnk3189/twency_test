import java.util.*;
public class ReverseString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter string to reverse:");
        
		ArrayList<String> reversedword = new ArrayList<String>();
		
        Scanner read = new Scanner(System.in);
        String[] str = read.nextLine().split(" ");
        
        for (String ss : str) {
			if (ss.length() >= 5){
				reversedword.add(Reverse(ss));
			}
			else {
				reversedword.add(ss);
			}			
        }
        	 System.out.print("Results:" + String.join(" ", reversedword));
        
    }
	
	
	
	public static String Reverse(String s)
	{
		StringBuilder sb = new StringBuilder();
        
        for(int i = s.length() - 1; i >= 0; i--)
        {
            sb.append(s.charAt(i));
        }
        
        return sb.toString();
	}

}
